package com.asobiczewski.bidpdfparser.core;

import java.io.IOException;
import java.io.InputStream;

import org.apache.pdfbox.pdmodel.PDDocument;

public class FileReader {

    public static void main(String[] args) throws IOException {
        PDDocument document = new PDDocument();

        try {
            InputStream resourceAsStream = FileReader.class.getResourceAsStream("BidExample.pdf");
            document = PDDocument.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            document.close();
        }

    }
}
