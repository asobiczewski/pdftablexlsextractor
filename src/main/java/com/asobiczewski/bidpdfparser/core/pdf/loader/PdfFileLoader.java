package com.asobiczewski.bidpdfparser.core.pdf.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDDocument;

public class PdfFileLoader implements FileLoader {

    @Override
    public Optional<PDDocument> load(String path) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File(path));
            PDDocument document = PDDocument.load(inputStream);
            return Optional.ofNullable(document);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(inputStream != null)
                inputStream.close();
        }
        return Optional.empty();
    }

}
