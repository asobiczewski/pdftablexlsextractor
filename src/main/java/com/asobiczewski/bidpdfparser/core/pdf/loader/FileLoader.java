package com.asobiczewski.bidpdfparser.core.pdf.loader;

import java.io.IOException;
import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDDocument;

public interface FileLoader {
    Optional<PDDocument> load(String path) throws IOException;
}
