package com.asobiczewski.bidpdfparser.ui.loader;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;

public class LoaderController implements Initializable {

    @FXML
    private BorderPane dropFilePane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void dragReleased(DragEvent e) {
        System.out.println("File dropped");
    }

    public void dragOver(DragEvent e) {
        Dragboard db = e.getDragboard();
        if (db.hasFiles()) {
            e.acceptTransferModes(TransferMode.ANY);
        } else {
            e.consume();
        }
    }
}
