package com.asobiczewski.bidpdfparser.core.pdf.loader;

import static org.junit.Assert.assertTrue;

import java.awt.geom.Rectangle2D;
import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFTextStripperByArea;
import org.junit.Before;
import org.junit.Test;

public class PdfFileLoaderTest {

    public static final String PATH = "/home/asobiczewski/project/personal/other/BidPdfParser/src/test/resources/BidExample.pdf";

    private FileLoader fileLoader;

    @Before
    public void setUp() throws Exception {
        fileLoader = new PdfFileLoader();
    }

    @Test
    public void load() throws Exception {
        Optional<PDDocument> load = fileLoader.load(PATH);
        assertTrue(load.isPresent());
        PDDocument pdDocument = load.get();
        PDFTextStripper stripper = new PDFTextStripper();
        PDFTextStripperByArea pdfTextStripperByArea = new PDFTextStripperByArea(){};
        pdfTextStripperByArea.addRegion("1", new Rectangle2D.Float(1,1,300,300));
        pdfTextStripperByArea.extractRegions((PDPage) pdDocument.getDocumentCatalog().getAllPages().get(1));
        String textForRegion = pdfTextStripperByArea.getTextForRegion("1");
        System.out.println(textForRegion);
    }
}